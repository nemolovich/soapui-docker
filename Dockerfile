FROM openjdk:11-slim
LABEL maintainer="Brian GOHIER<nemolovich@hotmail.fr>"

ARG SOAPUI_VERSION="5.7.1"

ENV SOAPUI_VERSION="${SOAPUI_VERSION}"
ENV SOAPUI_HOME="/opt/soapui"
ENV PATH="${SOAPUI_HOME}/bin:${PATH}"

RUN apt-get update && \
    apt-get install -y wget && \
    wget -q --show-progress https://dl.eviware.com/soapuios/${SOAPUI_VERSION}/SoapUI-${SOAPUI_VERSION}-linux-bin.tar.gz -O /tmp/soapui.tar.gz && \
    ls -lah /tmp/soapui.tar.gz && \
    tar -xvzf /tmp/soapui.tar.gz && \
    mv SoapUI-${SOAPUI_VERSION} ${SOAPUI_HOME} && \
    apt-get remove -y wget && \
    apt-get clean autoclean && \
    apt-get autoremove --yes && \
    rm -rf /var/lib/{apt,dpkg,cache,log}/
