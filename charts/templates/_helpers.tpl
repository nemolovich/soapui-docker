{{/*
Expand the name of the chart.
*/}}
{{- define "soapui.name" -}}
{{ default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "soapui.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "soapui.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Unmap from field
*/}}
{{- define "soapui.unmap" -}}
{{- $result := "" }}
{{- $matchItem := "" }}
{{- range $item := .map }}
  {{- range $k, $v := $item }}
    {{- if eq $k $.key.field }}
      {{- if eq $v $.key.value }}
        {{- $matchItem = $item }}
      {{- end }}
    {{- end }}
  {{- end }}
{{- end }}
{{- if $matchItem }}
  {{- range $k, $v := $matchItem }}
    {{- if eq $k $.field }}
      {{- $result = $v  }}
    {{- end }}
  {{- end }}
{{- end }}
{{- $result | default .default }}
{{- end }}

{{/*
Namespace
*/}}
{{- define "soapui.namespace" -}}
{{ default .Release.Namespace .Values.namespaceOverride }}
{{- end }}

{{/*
Selector
*/}}
{{- define "soapui.workloadselector" -}}
workload.user.cattle.io/workloadselector: apps.deployment-{{ include "soapui.namespace" .}}-{{ .Chart.Name }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "soapui.labels" -}}
helm.sh/chart: {{ include "soapui.chart" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
app.kubernetes.io/name: {{ include "soapui.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
app: {{ .Chart.Name }}
{{- end }}


{{/*
Create the name of the service account to use
*/}}
{{- define "soapui.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "soapui.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}
